<?php

namespace Drupal\vereinonline_auth;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Exception\TransferException;

/**
 * Vereinonline login common class.
 */
class VereinonlineCommon {

  /**
   * Vereinonline login form validation.
   */
  public static function userRegisterValidate($form, FormStateInterface &$form_state) {
    $config = \Drupal::configFactory()->get('vereinonline_auth.settings');

    if (strlen($config->get('api_url')) === 0
    || strlen($config->get('api_user')) === 0
    || strlen($config->get('api_password')) === 0) {
      // Vereinonline_auth not configured.
      \Drupal::logger('vereinonline_auth')->notice(t('vereinonline_auth is not configured.'));
      return;
    }

    /** @var \Drupal\user\Entity\User|bool $user */
    $user = user_load_by_name($form_state->getValue('name'));
    if ($user === FALSE
    || $user->hasRole($config->get('user_role'))) {
      // No local user or local user with Vereinonline user role.
      self::syncLogin($form_state);
    }
  }

  /**
   * Vereinonline login sync.
   */
  public static function syncLogin(FormStateInterface &$form_state) {
    $config = \Drupal::configFactory()->get('vereinonline_auth.settings');
    $response = self::checkLogin($form_state->getValue('name'), $form_state->getValue('pass'));
    if (isset($response['error'])
    && $response['error'] === ''
    && !isset($response['errorcode'])
    && isset($response['email'])) {
      $form_state->clearErrors();
      /** @var \Drupal\user\UserInterface|false */
      $user = user_load_by_name($form_state->getValue('name'));
      if ($user === FALSE) {
        $user = User::create();
        \Drupal::logger('vereinonline_auth')->notice('New user created @username.', ['@username' => $form_state->getValue('name')]);
      }
      $user->setPassword($form_state->getValue('pass'));
      // Only first email address, trimmed.
      $email = trim(explode(';', $response['email'])[0]);
      $user->setEmail($email);
      // This username must be unique and accept only a-Z,0-9, - _ @ .
      $user->setUsername($form_state->getValue('name'));
      $user->addRole($config->get('user_role'));
      $user->activate();
      // Save user account.
      $user->save();

      // Login as user.
      user_login_finalize($user);
      $form_state->set('uid', $user->id());
      $form_state->setValidationComplete(TRUE);
      return;
    }
    elseif (isset($response['error'])
    && $response['error'] === 'fehlerhaft') {
      // Username/password not valid at Vereinonline.
      $form_state->clearErrors();
      $form_state->setErrorByName('name', t('Unrecognized username or password. <a href=":passwordurl">Forgot your password?</a>', [':passwordurl' => $config->get('api_url') . '?action=start_passwortvergessen']));
      return;
    }
    elseif (isset($response['error'])
    && $response['error'] === 'nicht authentifiziert') {
      // Auth token not valid.
      \Drupal::logger('vereinonline_auth')->error('Auth token not valid.', ['@server' => $config->get('api_url')]);
      $form_state->clearErrors();
      $form_state->setErrorByName('name', t('No connection to password server. Please try again later.'));
      return;
    }
    else {
      if (isset($response['errorcode'])
      && $response['errorcode'] === 'noconnection') {
        // No connection to Vereinonline.
        if ($config->get('fallback_noconnection') === TRUE) {
          // Falling back to local.
          \Drupal::logger('vereinonline_auth')->warning('No connection to @server. Fallback to local user database.', ['@server' => $config->get('api_url')]);
          return;
        }
        else {
          // Failing, no fallback.
          \Drupal::logger('vereinonline_auth')->error('No connection to @server.', ['@server' => $config->get('api_url')]);
          $form_state->clearErrors();
          $form_state->setErrorByName('name', t('No connection to password server. Please try again later.'));
          return;
        }
      }
      else {
        \Drupal::logger('vereinonline_auth')->error('Request error with @server: @error (@errorcode)', [
          '@server' => $config->get('api_url'),
          '@error' => $response['error'] ?? 'defaulterror',
          '@errorcode' => $response['errorcode'] ?? 'defaulterror',
        ]);
        $form_state->clearErrors();
        $form_state->setErrorByName('name', t('Request error with server. Please try again later.'));
        return;
      }
    }
  }

  /**
   * Vereinonline check login.
   */
  public static function checkLogin($name, $password) {
    if (drupal_valid_test_ua() !== FALSE) {
      // PHPunit test.
      $config = \Drupal::configFactory()->get('vereinonline_auth.settings');
      if ($config->get('api_url') === 'https://vereinonline_noconnection.org/') {
        $response = json_decode(file_get_contents(dirname(__FILE__) . '/../tests/src/Functional/Mocks/noconnection.json'), TRUE);
      }
      elseif ($name === 'first.last' && $password === '7654321') {
        $response = json_decode(file_get_contents(dirname(__FILE__) . '/../tests/src/Functional/Mocks/richtigeruser.json'), TRUE);
      }
      else {
        $response = json_decode(file_get_contents(dirname(__FILE__) . '/../tests/src/Functional/Mocks/fehlerhaft.json'), TRUE);
      }
    }
    else {
      $response = self::request('CheckLogin', [
        'user' => $name,
        'password' => $password,
      ]);
    }
    return $response;
  }

  /**
   * Vereinonline request.
   */
  public static function request($command, $data) {
    $httpClient = \Drupal::httpClient();
    $config = \Drupal::configFactory()->get('vereinonline_auth.settings');
    $url = $config->get('api_url');
    $url .= '?json';
    $url .= '&function=' . $command;
    foreach ($data as $k => $v) {
      $url .= '&' . $k . '=' . urlencode($v);
    }
    $url .= '&token=A/' . $config->get('api_user') . '/' . md5($config->get('api_password'));
    try {
      $httpResponse = $httpClient->request('GET', $url, ['timeout' => 15]);
      $response = $httpResponse->getBody();
    }
    catch (ServerException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "servererror" }';
    }
    catch (ClientException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "clienterror" }';
    }
    catch (BadResponseException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "badresponseerror" }';
    }
    catch (TooManyRedirectsException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "toomanyerror" }';
    }
    catch (ConnectException $e) {
      $response = '{ "error": "No connection to vereinonline.org server. Please try again later.", "errorcode": "noconnection" }';
    }
    catch (RequestException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "requesterror" }';
    }
    catch (TransferException $e) {
      $response = '{ "error": $e->getResponse(), "errorcode": "transfererror" }';
    }
    if ($response === '[""]') {
      $response = '{ "error": "Invalid response from vereinonline.org server. Please try again later.", "errorcode": "noconnectionempty" }';
    }
    return json_decode($response, TRUE);
  }

}
