<?php

namespace Drupal\vereinonline_auth\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Vereinonline Settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The variable containing the role manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(
    EntityTypeManager $entityTypeManager,
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vereinonline_auth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vereinonline_auth.settings');
    /** @var \Drupal\user\RoleStorage */
    $roleStorage = $this->entityTypeManager->getStorage('user_role');

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vereinonline URL'),
      '#default_value' => $config->get('api_url'),
      '#required' => TRUE,
    ];
    $form['api_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API user'),
      '#default_value' => $config->get('api_user'),
      '#required' => TRUE,
    ];
    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API password'),
      '#default_value' => $config->get('api_password'),
      '#required' => TRUE,
    ];

    $form['fallback_noconnection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fallback to local user database when no connection to Vereinonline URL'),
      '#default_value' => $config->get('fallback_noconnection'),
    ];

    $filtered_roles = [];
    $ids = $roleStorage->getQuery()
      ->condition('id', 'anonymous', '<>')
      ->condition('id', 'authenticated', '<>')
      ->condition('id', 'administrator', '<>')
      ->execute();
    $all_roles = $roleStorage->loadMultiple($ids);
    /** @var \Drupal\user\RoleInterface $value */
    foreach ($all_roles as $key => $value) {
      $filtered_roles[$key] = $value->get('label');
    }
    $form['user_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Add role to new user'),
      '#description' => $this->t('"Administrator", "Authenticated" and "Anonymous user" roles are excluded.'),
      '#default_value' => $config->get('user_role'),
      '#required' => TRUE,
      '#options' => $filtered_roles,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('vereinonline_auth.settings');
    $keys = [
      'api_url',
      'api_user',
      'api_password',
      'user_role',
      'fallback_noconnection',
    ];
    foreach ($keys as $key) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();
  }

}
