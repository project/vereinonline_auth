CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Vereinonline Auth module allows users to authenticate Drupal resources
through the Vereinonline login mechanism.


 * For a full description of the module visit:
   https://www.drupal.org/project/vereinonline_auth

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/vereinonline_auth


REQUIREMENTS
------------

This module requires an API account with Vereinonline.org .


INSTALLATION
------------

Install the Vereinonline Auth module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Vereinonline Auth
       module.
    2. Navigate to Administration > Configuration > People > Vereinonline auth
       settings to edit settings.
    3. Enter the Vereinonline URL, API user and API password.
    4. Select a role for new users from Vereinonline. Users with
       "Administrator", "Anonymous" and "Authenticated" roles are excluded.
       An additional role is required.
    5. Save configuration.


TROUBLESHOOTING
---------------


FAQ
---



MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
