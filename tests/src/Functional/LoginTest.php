<?php

namespace Drupal\Tests\vereinonline_auth\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Tests that the condition plugins work.
 *
 * @group vereinonline_auth
 */
class LoginTest extends BrowserTestBase {
  use SchemaCheckTestTrait;

  const API_URL = 'https://www.vereinonline.org/vereinonlinetest/';
  const API_URL_WRONG = 'https://vereinonline_noconnection.org/';
  const API_USER = 'vereinonlinetest';
  const API_PASSWORD = 'testvereinonline';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'vereinonline_auth',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the minimal profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $admin;

  /**
   * Normal user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $normalUser;

  /**
   * Vereinonline role.
   *
   * @var string
   */
  protected $role = '';

  /**
   * Vereinonline node page.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodePage;

  /**
   * Vereinonline node article.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodeArticle;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);

    // Create role + user.
    $this->role = $this->drupalCreateRole([], NULL);
    $this->normalUser = $this->drupalCreateUser(['view own unpublished content'], NULL, FALSE);

    // Module settings.
    $this->drupalGet('/admin/config/people/vereinonline_auth');
    $this->submitForm([
      'api_url' => self::API_URL,
      'api_user' => self::API_USER,
      'api_password' => self::API_PASSWORD,
      'fallback_noconnection' => TRUE,
      'user_role' => $this->role,
    ], 'Submit');

    $this->drupalLogout();
  }

  /**
   * Test the settings.
   */
  public function testSettings() {
    $this->drupalLogin($this->admin);
    $this->drupalGet('/admin/config/people/vereinonline_auth');
    $this->assertSession()->fieldValueEquals('api_url', self::API_URL);
    $this->assertSession()->fieldValueEquals('api_user', self::API_USER);
    $this->assertSession()->fieldValueEquals('api_password', self::API_PASSWORD);
    $this->assertSession()->fieldValueEquals('user_role', $this->role);
    $this->drupalLogout();
  }

  /**
   * Test the page login.
   */
  public function testPageLogin() {

    // Is not logged in as user.
    $this->drupalGet('/user');
    $this->assertSession()->addressEquals('/user/login');

    // Logs in as normal user.
    $this->drupalLogin($this->normalUser);
    $this->drupalGet('/user');
    $this->assertSession()->addressEquals('/user/' . $this->normalUser->id());
    $this->drupalLogout();

    // Fail with unknown user and wrong password.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'userfehlerhaft',
      'pass' => '1234567',
    ], 'Log in');
    $this->assertSession()->pageTextContains('Unrecognized username or password.');
    $this->assertSession()->addressEquals('/user/login');

    // Fail with user and wrong password.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '1234567',
    ], 'Log in');
    $this->assertSession()->pageTextContains('Unrecognized username or password.');
    $this->assertSession()->addressEquals('/user/login');

    // Create new user and login.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '7654321',
    ], 'Log in');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password.');
    $this->assertSession()->addressNotEquals('/user/login');
    $this->drupalLogout();

    // Fail with new user and wrong password.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '1234567',
    ], 'Log in');
    $this->assertSession()->pageTextContains('Unrecognized username or password.');
    $this->assertSession()->addressEquals('/user/login');
  }

  /**
   * Test the no connection situation with fallback.
   */
  public function testNoConnectionFallback() {
    // Create new user and login.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '7654321',
    ], 'Log in');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password.');
    $this->assertSession()->addressNotEquals('/user/login');
    $this->drupalLogout();

    $this->drupalLogin($this->admin);
    // Set no connection URL.
    $this->drupalGet('/admin/config/people/vereinonline_auth');
    $this->submitForm([
      'api_url' => self::API_URL_WRONG,
      'api_user' => self::API_USER,
      'api_password' => self::API_PASSWORD,
      'fallback_noconnection' => TRUE,
      'user_role' => $this->role,
    ], 'Submit');
    $this->drupalLogout();

    // Login and test connection.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '7654321',
    ], 'Log in');
    $this->assertSession()->pageTextNotContains('No connection to password server. Please try again later.');
    $this->assertSession()->addressNotEquals('/user/login');
    $this->drupalLogout();

    // Logs in as normal user.
    $this->drupalLogin($this->normalUser);
    $this->assertSession()->pageTextNotContains('No connection to password server. Please try again later.');
    $this->assertSession()->addressNotEquals('/user/login');

    $this->drupalLogout();
  }

  /**
   * Test the no connection situation without fallback.
   */
  public function testNoConnectionNoFallback() {
    // Create new user and login.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '7654321',
    ], 'Log in');
    $this->assertSession()->pageTextNotContains('Unrecognized username or password.');
    $this->assertSession()->addressNotEquals('/user/login');
    $this->drupalLogout();

    $this->drupalLogin($this->admin);
    // Set no connection URL.
    $this->drupalGet('/admin/config/people/vereinonline_auth');
    $this->submitForm([
      'api_url' => self::API_URL_WRONG,
      'api_user' => self::API_USER,
      'api_password' => self::API_PASSWORD,
      'fallback_noconnection' => FALSE,
      'user_role' => $this->role,
    ], 'Submit');
    $this->drupalLogout();

    // Login and test connection.
    $this->drupalGet('/user/login');
    $this->submitForm([
      'name' => 'first.last',
      'pass' => '7654321',
    ], 'Log in');
    $this->assertSession()->pageTextContains('No connection to password server. Please try again later.');
    $this->assertSession()->addressEquals('/user/login');

    // Logs in as normal user.
    $this->drupalLogin($this->normalUser);
    $this->assertSession()->pageTextNotContains('No connection to password server. Please try again later.');
    $this->assertSession()->addressNotEquals('/user/login');

    $this->drupalLogout();
  }

}
